# Portfolio

Nous devions réaliser un portfolio. Je suis donc partis sur quelque chose de simple et de sobre, avec une image fixe comme en tête, et 3 sections faisant chacune la taille d'un écran, accessibles en scrollant ou via un menu burger en haut à droite.


![alt text](img/Portfolio1.png)
![alt text](img/Portfolio2.png)

**Lien:** https://qdurand96.gitlab.io/Portfolio
